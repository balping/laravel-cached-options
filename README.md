# Laravel Cached Options

Simple wrapper around [`overtrue/laravel-options`](https://github.com/overtrue/laravel-options) global options module for Laravel application to provide basic caching functionality.

## Installation

First disable auto-discovery for `overtrue/laravel-options`:

In `composer.json` add:

```json
   "extra": {
        "laravel": {
            "dont-discover": [
	            "overtrue/laravel-options"
            ]
        }
    }
```

Then install the package via composer:

```bash
composer require balping/laravel-cached-options
```

### Publish configuration and migrations

```bash
$ php artisan vendor:publish --provider="Balping\LaravelCachedOptions\CachedOptionsServiceProvider"
```

### Run migrations

```bash
$ php artisan migrate
```

## Configuration

In `config/cache.php` configure a driver that supports tags. **Warning!** File,
dynamodb, or database drivers are not supported.

In `config/options.php` you can choose a TTL for cached values. TTL of `null` means forever.

## Usage

Exactly the same way as described in [`overtrue/laravel-options` documentation](https://github.com/overtrue/laravel-options#usage), but all the values are going to be cached.

## License

MIT