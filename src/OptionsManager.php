<?php

namespace Balping\LaravelCachedOptions;

class OptionsManager extends \Overtrue\LaravelOptions\OptionsManager implements \Overtrue\LaravelOptions\Contracts\Option {
	protected array $customCreators = [
		'cached' => [self::class, 'createCachedEloquentProvider'],
	];

	public static function createCachedEloquentProvider($app, $config){
		return new CachedOptionProvider($app[$config['model']]);
	}
}
