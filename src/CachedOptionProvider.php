<?php

namespace Balping\LaravelCachedOptions;

use Illuminate\Support\Facades\Cache;
use Overtrue\LaravelOptions\Contracts\OptionProvider;
use Overtrue\LaravelOptions\EloquentProvider;

class CachedOptionProvider extends EloquentProvider implements OptionProvider {
	public function has(string $key): bool{
		return Cache::tags(['options', 'options.has'])
			->remember($key, $this->ttl(), function() use ($key){
				return parent::has($key);
			});
	}

	public function getAll(array $keys = []): array{
		$hash = md5(serialize($keys));
		return Cache::tags(['options', 'options.all'])
			->remember($hash, $this->ttl(), function() use ($keys){
				return parent::getAll($keys);
			});
	}

	public function get(string $key, $default = null){
		return Cache::tags(['options', 'options.key'])
			->remember($key, $this->ttl(), function() use ($key, $default){
				return parent::get($key, $default);
			});
	}

	public function set(string $key, $value): OptionProvider{
		parent::set($key, $value);
		$this->flushForKey($key);
		return $this;
	}

	public function multiSet(array $options): OptionProvider{
		parent::multiSet($options);
		Cache::tags('options')->flush();
		return $this;
	}

	public function remove(string $key): OptionProvider{
		parent::remove($key);
		$this->flushForKey($key);
		return $this;
	}

	public function multiRemove(array $keys): OptionProvider{
		parent::multiRemove($keys);
		Cache::tags('options')->flush();
		return $this;
	}

	private function flushForKey(string $key){
		Cache::tags(['options', 'options.has'])->forget($key);
		Cache::tags(['options', 'options.key'])->forget($key);
		Cache::tags('options.all')->flush();
	}

	private function ttl(){
		return config('options.providers.cached.ttl', 60);
	}
}

