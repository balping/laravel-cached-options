<?php

return [
    'defaults' => [
        'provider' => 'cached',
    ],

    'providers' => [
        'eloquent' => [
            'driver' => 'eloquent',
            'model' => \Overtrue\LaravelOptions\Option::class,
        ],
        'cached' => [
            'driver' => 'cached',
            'model' => \Overtrue\LaravelOptions\Option::class,
            'ttl' => 120*60, // cache TTL in seconds. null means forever.
        ],
    ],
];

